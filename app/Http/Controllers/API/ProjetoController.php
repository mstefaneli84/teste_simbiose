<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Projeto;
use App\Http\Resources\ProjetoResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjetoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $callback = function ($query) use($request) {

            $campos_like = ['nome', 'segmento', 'area', 'municipio', 'situacao', 'mecanismo', 'enquadramento' , 'valor_captado', 'valor_aprovado', 'acessibilidade', 'objetivos', 'justificativa', 'etapa', 'ficha_tecnica', 'impacto_ambiental', 'especificacao_tecnica', 'providencia', 'democratizacao', 'sinopse', 'resumo', 'valor_projeto', 'outras_fontes', 'valor_proposta', 'valor_solicitado', 'objetivo', 'estrategia_execucao', 'link_incentivadores'];

            foreach ($request->only($campos_like) as $name => $value)
            {
                $value && $query->where($name, 'LIKE', "%{$value}%");
            }


            $campos_equal = ['pronac', 'ano_projeto', 'uf', 'data_inicio', 'data_termino', 'created_at', 'updated_at'];

            foreach ($request->only($campos_equal) as $name => $value) {

                $value && $query->where([$name => $value]);
            }

        };

        $projetos = Projeto::orderBy('ano_projeto')->where($callback)->paginate(15)->appends($request->all());


        //$projetos = Projeto::all();
        return response([ 'projetos' => ProjetoResource::collection($projetos), 'message' => 'Retrieved successfully'], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        

        $validator = Validator::make($data, [
            'pronac' => 'required|max:191',
            'ano_projeto' => 'required|max:191',
            'nome' => 'required|max:191',
            'segmento' => 'required|max:191',
            'area' => 'required|max:191',
            'uf' => 'required|max:191',
            'municipio' => 'required|max:191',
            'data_inicio' => 'required|max:191',
            'data_termino' => 'required|max:191',
            'situacao' => 'required|max:191',
            'mecanismo' => 'required|max:191',
            'enquadramento' => 'required|max:191',
            'valor_captado' => 'required|max:191',
            'valor_aprovado' => 'required|max:191',
            'acessibilidade' => 'required',
            'objetivos' => 'required',
            'justificativa' => 'required',
            'etapa' => 'required',
            'ficha_tecnica' => 'required',
            'impacto_ambiental' => 'required',
            'especificacao_tecnica' => 'required',
            'providencia' => 'required',
            'democratizacao' => 'required',
            'sinopse' => 'required',
            'resumo' => 'required',
            //'created_at' => 'required',
            //'updated_at' => 'required',
            'valor_projeto' => 'required|max:100',
            'outras_fontes' => 'required|max:100',
            'valor_proposta' => 'required|max:100',
            'valor_solicitado' => 'required|max:100',
            'objetivo' => 'required',
            'estrategia_execucao' => 'required',
            'link_incentivadores' => 'required|max:255'
        ]);

        if($validator->fails()){
            return response(['error' => $validator->errors(), 'Validation Error']);
        }


        $data['id_projeto'] = Projeto::all()->max('id_projeto') + 1;


        $projeto = Projeto::create($data);

        return response([ 'projeto' => new ProjetoResource($projeto), 'message' => 'Created successfully'], 200);
 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Projeto  $projeto
     * @return \Illuminate\Http\Response
     */
    public function show(Projeto $projeto)
    {
    
         return response([ 'projeto' => new ProjetoResource($projeto), 'message' => 'Retrieved successfully'], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Projeto  $projeto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Projeto $projeto)
    {
        $projeto->update($request->all());

        return response([ 'projeto' => new ProjetoResource($projeto), 'message' => 'Retrieved successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Projeto  $projeto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Projeto $projeto)
    {
        $projeto->delete();

        return response(['message' => 'Deleted']);
    }
}
